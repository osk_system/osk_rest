from django.conf.urls import url, include
from rest_framework import routers
from core.views import SchoolViewSet, StudentViewSet, InstructorViewSet, OfficeViewSet, TimetableViewSet, RideViewSet

router = routers.DefaultRouter()
router.register(r'school', SchoolViewSet)
router.register(r'student', StudentViewSet)
router.register(r'instructor', InstructorViewSet)
router.register(r'office', OfficeViewSet)
router.register(r'timetable', TimetableViewSet)
router.register(r'ride', RideViewSet)

urlpatterns = [
    url(r'^docs/', include('rest_framework_swagger.urls')),
]
urlpatterns += router.urls
