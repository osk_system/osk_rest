#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils.translation import ugettext as _
from django.contrib.auth.models import BaseUserManager


class OSKUserManager(BaseUserManager):

    def create_user(self, email, password=None, **kwargs):
        user = self.model(email=email, **kwargs)
        user.user_type = "ST"
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None, **kwargs):
        user = self.model(email=email, **kwargs)
        user.user_type = "SA"
        user.is_superuser = True
        user.set_password(password)
        user.save()
        return user


class School(models.Model):
    name = models.CharField(max_length=50, verbose_name=_(u"Nazwa"))

    class Meta:
        verbose_name = _(u"Szkoła jazdy")
        verbose_name_plural = _(u"Szkoły jazdy")

    def __unicode__(self):
        return self.name


class OSKUser(AbstractBaseUser):
    TYPE_CHOICES = (
        ('SA', u'Super Admin'),
        ('OF', u'Biuro'),
        ('IN', u'Instruktor'),
        ('ST', u'Kursant'),
    )

    first_name = models.CharField(_(u"Imię"), max_length=30, null=False, blank=False)
    last_name = models.CharField(_(u"Nazwisko"), max_length=30, null=False, blank=False)
    email = models.EmailField(_(u"E-Mail"), null=False, blank=False, unique=True)
    phone = models.CharField(max_length=100, verbose_name=_(u"Telefon"), blank=True, default="")
    school = models.ForeignKey(School, null=True, blank=True, verbose_name=_(u"Szkoła jazdy"))
    user_type = models.CharField(max_length=2, choices=TYPE_CHOICES, default="ST",
                                 verbose_name=_(u"Rodzaj użytkownika"))
    pesel = models.CharField(max_length=11, verbose_name=_(u"PESEL"), null=True, blank=True)
    pkk_no = models.CharField(max_length=20, verbose_name=_(u"Numer PKK"), null=True, blank=True)
    is_superuser = models.BooleanField(_(u"Super Administrator"), default=False, editable=False)

    USERNAME_FIELD = "email"
    objects = OSKUserManager()

    @property
    def is_staff(self):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, app_label):
        return True

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    class Meta:
        verbose_name = _(u"Użytkownik")
        verbose_name_plural = _(u"Użytkownicy")

    def __str__(self):
        return self.get_full_name()


class Timetable(models.Model):
    instructor = models.ForeignKey(OSKUser, verbose_name=_(u"Instruktor"))

    class Meta:
        verbose_name = _(u"Terminarz")
        verbose_name_plural = _(u"Terminarze")


class Ride(models.Model):
    timetable = models.ForeignKey(Timetable, verbose_name=_(u"Terminarz"))
    instructor = models.ForeignKey(OSKUser, verbose_name=_(u"Instruktor"))
    start = models.DateTimeField(verbose_name=_(u"Początek jazd"))
    end = models.DateTimeField(verbose_name=_(u"Koniec jazd"))
    done = models.BooleanField(verbose_name=_(u"Odbyte?"))

    class Meta:
        verbose_name = _(u"Jazda")
        verbose_name_plural = _(u"Jazdy")
