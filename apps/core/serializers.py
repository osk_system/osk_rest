from rest_framework import serializers
from core.models import School, OSKUser, Timetable, Ride


class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = School


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'email', 'first_name', 'last_name', 'phone', 'user_type', 'school', 'pesel', 'pkk_no')
        model = OSKUser


class InstructorSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'email', 'first_name', 'last_name', 'phone', 'user_type', 'school')
        model = OSKUser


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'email', 'first_name', 'last_name', 'phone', 'user_type', 'school')
        model = OSKUser


class TimetableSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Timetable


class RideSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Ride
