from rest_framework import viewsets
from core.permissions import TestPermission
from core.models import School, OSKUser, Timetable, Ride
from core.serializers import (SchoolSerializer, StudentSerializer, InstructorSerializer, OfficeSerializer,
                              TimetableSerializer, RideSerializer)


class SchoolViewSet(viewsets.ModelViewSet):
    queryset = School.objects.all()
    serializer_class = SchoolSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = OSKUser.objects.filter(user_type='ST')
    serializer_class = StudentSerializer
    permission_classes = (TestPermission,)


class InstructorViewSet(viewsets.ModelViewSet):
    queryset = OSKUser.objects.filter(user_type='IN')
    serializer_class = InstructorSerializer


class OfficeViewSet(viewsets.ModelViewSet):
    queryset = OSKUser.objects.filter(user_type='OF')
    serializer_class = OfficeSerializer


class TimetableViewSet(viewsets.ModelViewSet):
    queryset = Timetable.objects.all()
    serializer_class = TimetableSerializer


class RideViewSet(viewsets.ModelViewSet):
    queryset = Ride.objects.all()
    serializer_class = RideSerializer
