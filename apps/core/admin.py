from django.contrib import admin
from core.models import School, OSKUser, Timetable, Ride

admin.site.register(School)
admin.site.register(OSKUser)
admin.site.register(Timetable)
admin.site.register(Ride)
