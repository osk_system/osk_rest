from rest_framework import permissions


class TestPermission(permissions.BasePermission):
    message = "Nie masz uprawnien"

    def has_permission(self, request, view):
        # global check for permission
        print(request)
        return True

    def has_object_permission(self, request, view, obj):
        # checking object for permission

        # GET, HEAD or OPTIONS
        if request.method in permissions.SAFE_METHODS:
            return True

        return False
